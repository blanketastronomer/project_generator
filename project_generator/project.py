import os
from textwrap import dedent
from pathlib import Path

class Project(object):
    def __init__(self, project_name):
        self.name = project_name
        self.main_package_directory = os.path.join(self.name, self.name)
        self.main_package_init_file = os.path.join(self.main_package_directory, '__init__.py')
        self.setup_py = os.path.join(self.name, 'setup.py')
        self.cli_py = os.path.join(self.main_package_directory, 'cli.py')
        self.gitignore = os.path.join(self.name, '.gitignore')

    def create_project_directory(self):
        try:
            os.mkdir(self.name)
        except FileExistsError:
            print("Project directory already exists.  Continuing...")

    def create_python_package(self):
        try:
            os.mkdir(self.main_package_directory)
            self.touch_file(self.main_package_init_file)
        except FileExistsError:
            print("Main package already exists.  Moving on...")

    def touch_file(self, path):
        Path(path).touch()

    def create_cli_py(self):
        text = f'''
        import click
        import os
        
        @click.group()
        @click.version_option(version='0.1')
        def cli():
            pass
        
        @cli.command(help-'HELP MESSAGE')
        @click.argument('name')
        @click.option('--activate-something', is_flag=True, default=True, help='ACTIVATE SOMETHING')
        def example_command(name, **kwargs):
            pass
        '''

        self.create_file(self.cli_py, text)

    def create_setup_py(self):
        min_python_version = os.popen('python --version').read()
        _, min_python_version = min_python_version.split(' ')

        text = f'''
        from setuptools import setup, find_packages
        
        with open('README.rst', 'r') as readme_file:
            long_description = readme_file.read()
            
        setup(
            name='{self.name}',
            version='0.1',
            description='[DESCRIPTION_GOES_HERE]',
            url='[VCS_URL_GOES_HERE]',
            author='[AUTHOR_NAME_GOES_HERE]',
            author_email='[AUTHOR_EMAIL_GOES_HERE]',
            license='MIT',
            packages=find_packages(),
            python_requires=">={min_python_version.strip()}",
            # Packages to be bundled with this package.
            install_requires=[
                'click'
            ],
            # Entry points (commandline executables)
            entry_points={chr(ord('{'))}
                'console_scripts': [
                    '{self.name} = {self.name}.cli:cli'
                ]
            {chr(ord('}'))}
        )
        '''

        self.create_file(self.setup_py, text)

    def create_file(self, path, text):
        path = self.expand_path(path)

        with open(path, 'w') as file:
            contents = dedent(text).strip()

            print(contents, file=file)

    def create_gitignore(self):
        text = '''
        # Covers JetBrains IDEs: IntelliJ, RubyMine, PhpStorm, AppCode, PyCharm, CLion, Android Studio and Webstorm
        # Reference: https://intellij-support.jetbrains.com/hc/en-us/articles/206544839
        
        # User-specific stuff:
        .idea/workspace.xml
        .idea/tasks.xml
        .idea/dictionaries
        .idea/vcs.xml
        .idea/jsLibraryMappings.xml
        
        # Sensitive or high-churn files:
        .idea/dataSources.ids
        .idea/dataSources.xml
        .idea/dataSources.local.xml
        .idea/sqlDataSources.xml
        .idea/dynamic.xml
        .idea/uiDesigner.xml
        
        # Gradle:
        .idea/gradle.xml
        .idea/libraries
        
        # Mongo Explorer plugin:
        .idea/mongoSettings.xml
        
        ## File-based project format:
        *.iws
        
        ## Plugin-specific files:
        
        # IntelliJ
        /out/
        
        # mpeltonen/sbt-idea plugin
        .idea_modules/
        
        # JIRA plugin
        atlassian-ide-plugin.xml
        
        # Crashlytics plugin (for Android Studio and IntelliJ)
        com_crashlytics_export_strings.xml
        crashlytics.properties
        crashlytics-build.properties
        fabric.properties
        
        *~
        
        # temporary files which can be created if a process still has a handle open of a deleted file
        .fuse_hidden*
        
        # KDE directory preferences
        .directory
        
        # Linux trash folder which might appear on any partition or disk
        .Trash-*
        
        *.DS_Store
        .AppleDouble
        .LSOverride
        
        # Icon must end with two \r
        Icon
        
        
        # Thumbnails
        ._*
        
        # Files that might appear in the root of a volume
        .DocumentRevisions-V100
        .fseventsd
        .Spotlight-V100
        .TemporaryItems
        .Trashes
        .VolumeIcon.icns
        .com.apple.timemachine.donotpresent
        
        # Directories potentially created on remote AFP share
        .AppleDB
        .AppleDesktop
        Network Trash Folder
        Temporary Items
        .apdisk
        
        # Byte-compiled / optimized / DLL files
        __pycache__/
        *.py[cod]
        *$py.class
        
        # C extensions
        *.so
        
        # Distribution / packaging
        .Python
        env/
        build/
        develop-eggs/
        dist/
        downloads/
        eggs/
        .eggs/
        lib/
        lib64/
        parts/
        sdist/
        var/
        *.egg-info/
        .installed.cfg
        *.egg
        
        # PyInstaller
        #  Usually these files are written by a python script from a template
        #  before PyInstaller builds the exe, so as to inject date/other infos into it.
        *.manifest
        *.spec
        
        # Installer logs
        pip-log.txt
        pip-delete-this-directory.txt
        
        # Unit test / coverage reports
        htmlcov/
        .tox/
        .coverage
        .coverage.*
        .cache
        nosetests.xml
        coverage.xml
        *,cover
        .hypothesis/
        
        # Translations
        *.mo
        *.pot
        
        # Django stuff:
        *.log
        local_settings.py
        
        # Flask stuff:
        instance/
        .webassets-cache
        
        # Scrapy stuff:
        .scrapy
        
        # Sphinx documentation
        docs/_build/
        
        # PyBuilder
        target/
        
        # IPython Notebook
        .ipynb_checkpoints
        
        # pyenv
        .python-version
        
        # celery beat schedule file
        celerybeat-schedule
        
        # dotenv
        .env
        
        # virtualenv
        venv/
        ENV/
        
        # Spyder project settings
        .spyderproject
        
        # Rope project settings
        .ropeproject
        
        # Windows image file caches
        Thumbs.db
        ehthumbs.db
        
        # Folder config file
        Desktop.ini
        
        # Recycle Bin used on file shares
        $RECYCLE.BIN/
        
        # Windows Installer files
        *.cab
        *.msi
        *.msm
        *.msp
        
        # Windows shortcuts
        *.lnk
        '''

        self.create_file(self.gitignore, text)

    def expand_path(self, path):
        return os.path.expandvars(path)
