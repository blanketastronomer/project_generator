import click
import os
import shutil
from project_generator.project import Project

@click.group()
@click.version_option(version='0.1')
def cli():
    pass

def generate_commandline(project_name):
    cmdline = Project(project_name)
    cmdline.create_project_directory()
    cmdline.create_python_package()
    cmdline.create_setup_py()
    cmdline.create_cli_py()
    cmdline.touch_file(os.path.join(cmdline.name, 'LICENSE'))
    cmdline.touch_file(os.path.join(cmdline.name, 'README.rst'))
    cmdline.create_gitignore()

def generate_gui():
    pass

@click.argument('name')
@cli.command(help="Generate a new project")
@click.option('--gui', is_flag=True, default=False, help="Generate a GUI app")
def generate(name, **kwargs):
    if kwargs['gui']:
        generate_gui()
    else:
        generate_commandline(name)

@cli.command(help="Destroy a project")
@click.argument('name')
def destroy(name, **kwargs):
    if os.path.exists(name):
        try:
            shutil.rmtree(name)
        except OSError as e:
            print(f"Error: {e.filename} - {e.strerror}")
    else:
        print(f"Directory not found: {name}")
