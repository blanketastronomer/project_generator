project_generator
=================

Generates new Python commandline and GUI boilerplate.

Installation
============

Via pip:

.. code-block:: bash

    $ pip install git+https://gitlab.com/blanketastronomer/project_generator.git

Usage
=====

Getting a list of all options:

.. code-block:: bash

    $ project_generator --help

Generate a new project named 'example_project':

.. code-block:: bash

    $ project_generator generate example_project

Destroy an existing project called 'example_project':

.. code-block:: bash

    $ project_generator destroy example_project

Contributing
============

Issues and pull requests are welcome on Gitlab at https://gitlab.com/blanketastronomer/project_generator

License
=======

MIT License

Copyright (c) 2018 Nicholas S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

