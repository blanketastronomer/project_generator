from setuptools import setup, find_packages

with open('README.rst', 'r') as readme_file:
    long_description = readme_file.read()

setup(
    name='project_generator',
    version='0.1',
    description='Generates new Python commandline and GUI boilerplate.',
    url='https://gitlab.com/blanketastronomer/project_generator',
    author='blanketastronomer <Nicholas S.>',
    author_email='nicksmrekar@gmail.com',
    license='MIT',
    packages=find_packages(),
    python_requires='>=3.0',
    # Add packages to be bundled with this package
    install_requires=[
        'click'
    ],
    # Add entry points (commandline executables)
    entry_points={
        'console_scripts': [
            'project_generator = project_generator.cli:cli'
        ]
    }
)
